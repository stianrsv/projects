
f = load('Project1_c2_.dat');

figure;
plot(log10(f(:,1)), f(:,2));

legend();
title('Oppgave c');
xlabel('h-step length, log10');
ylabel('abs(max error)');
print -dpng project1_c2_.png ;