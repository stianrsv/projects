// Project1 c
/* This program compute a numerical and an analytical solution for our given problem,
   compute and extract the maximum error in the numerical solution for different values of n
  and the computed values are written to another file to make a plot of the results
*/

#include <iostream>
#include <math.h>
#include <fstream>
#include <iomanip>

using namespace std;

int main()
{
    int i, j;

    // Open a writeabel file in the build directory for this program
    fstream outfile;
    outfile.open("Project1_c2_.dat", ios::out);

    // The loop let us run the program for many different values of n (different step length h)
    for(j = 1; j<=150; j++){

        //Declare local variables
        int M;
        M = pow(10,j/10.0);

        int n;
        n = M;
        double h, h2f;
        h = 1.0/(n+1);
        h2f = pow(h,2.0);

        // Makes vectors from 0 to n+1 with dynamic memory allocation
        double *a = new double[n+2];
        double *b = new double[n+2];
        double *c = new double[n+2];
        double *f = new double[n+2];
        double *v = new double[n+2];
        double *f_analytic = new double[n+2];
        long double *error = new long double[n+2];
        long double error_max;

        // Fills up the three vector in our matrix with values
        for(i=1; i <= n+1; i++){
            a[i] = -1;
            b[i] = 2;
            c[i] = -1;
            f[i] = h2f*100*exp(-10*i*h);
        }

        // Forward substitution with our own algorithm
        for(i=2; i <= n; i++){
            b[i] = b[i]-(a[i]*c[i-1])/b[i-1];
            f[i] = f[i]-(a[i]*f[i-1])/b[i-1];
        }

        // Set initial conditions
        v[0] = 0;
        f_analytic[0] = 0;

        // Backward substitution with our own algorithm
        v[n] = f[n]/b[n];

        for(i=n; i >= 0; i--){
            v[i-1] = (f[i-1]+v[i])/b[i-1];
        }

        // Compute the exact analytical solution to our differensial equation
        for(i=1; i <=n+1; i++){
            f_analytic[i] = 1 -(1-exp(-10))*i*h - exp(-10*i*h);
        }

        // Compute the error between numerical and anlytic solution for the differensial equation
        for(i=1; i <= n+1; i++){
            error[i] = log10((fabs(v[i-1]-f_analytic[i-1]))/f_analytic[i-1]);
            error[0] = 0;
            error[n+1] = 0;
        }

        // Execute the largest numerical error for each n-value
        error_max = error[1];
        for(i=1; i<=n+1; i++){
            if (error[i] < error_max){
                error_max = error[i];
            }
        }
        outfile << h << " " << error_max << endl;
        cout << "Step Length h: " << h << "  " << "Max error: " << setprecision(10) << error_max << endl;
    }
     // Closing the writeabel file in the build directory for this program
    outfile.close();

    return 0;
}

