// Project1 d
/* This program compute a numerical solution, using our own algorithm, for our given problem,
   solves the same problem using LU-decomposition in Armadillo,
   and estimate the time spent on solving our algotithm and the LU-decomposition
   to compare the effectiviness of our own algorithm with standard solving methods
*/

#include <iostream>
#include <math.h>
#include <fstream>
#include <iomanip>
#include <time.h>
#include <armadillo>

// Allows us to use the Armadillo library in this program
using namespace arma;
using namespace std;

int main()
{
    int i, j;

    for(j = 1; j<=3; j++){

        //Declare local variables
        int M;
        M = pow(10,j);

        int n;
        n = M;
        double h, h2f;
        h = 1.0/(n+1);
        h2f = pow(h,2.0);

        // Makes vectors from 0 to n+1 with dynamic memory allocation
        double *a = new double[n+2];
        double *b = new double[n+2];
        double *c = new double[n+2];
        double *f = new double[n+2];
        double *v = new double[n+2];
        double *f_analytic = new double[n+2];
        // Declear variables for time
        double time1, time2;

        // Define vectors and matrices using Armadillo
        mat A_mat(n,n);
        mat L_mat(n,n);
        mat U_mat(n,n);

        vec A(n-1);
        vec C(n-1);
        vec f2(n);

        // Make the matrix A

        // Eye is the identity matrix
        A_mat = 2*eye(n,n)*h2f;

        // "fill" set all elements in the vector/matrix to the given value
        A.fill(-1);
        C.fill(-1);

        // "diag" is the reference of the main diagonal at diag(0)
        A_mat.diag(-1) = A;
        A_mat.diag(1) = C;

        // Start clock to compute spent time
        clock_t start2, finish2;
        start2 = clock();

        // Armadillo factorize A into L and U
        lu(L_mat, U_mat, A_mat);

        // Armadillo solve the differensial equation
        vec Ly = solve(L_mat, f2);
        vec v2 = solve(U_mat, Ly);

        // Stop the clock and estimate the spent time
        finish2 = clock();
        time2 = ((finish2 - start2)/((double) CLOCKS_PER_SEC));


        // Doing the same numerical calculation with the same algorithm as in the previous programs in the project, in the rest of this program
        for(i=1; i <= n+1; i++){
            a[i] = -1;
            b[i] = 2;
            c[i] = -1;
            f[i] = h2f*100*exp(-10*i*h);
        }

        for(i=2; i <= n; i++){
            b[i] = b[i]-(a[i]*c[i-1])/b[i-1];
            f[i] = f[i]-(a[i]*f[i-1])/b[i-1];
        }

        v[0] = 0;
        f_analytic[0] = 0;

        v[n] = f[n]/b[n];

        clock_t start1, finish1;
        start1 = clock();

        for(i=n; i >= 0; i--){
            v[i-1] = (f[i-1]+v[i])/b[i-1];
        }

        for(i=1; i <=n+1; i++){
            f_analytic[i] = 1 -(1-exp(-10))*i*h - exp(-10*i*h);
        }

        finish1 = clock();
        time1 = ((finish1 - start1)/((double) CLOCKS_PER_SEC));

        cout << "n = 10^" << j << endl;
        cout << "Time algorithm: " << time1 << "s" << endl << "Time Armadillo LU: " << time2 << "s"<< endl << endl;
    }
    return 0;
}




