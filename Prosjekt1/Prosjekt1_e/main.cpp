// Project1 e
/* This program set up random matrices for multiplication,
   and doing the same multipliation six times,
   but with different order of the indexes i, j and k in the trippel for-loops,
   and makes us able to compare the the time spent with different index-order.
*/

#include <iostream>
#include <math.h>
#include <time.h>
#include <armadillo>

// Allows us to use the Armadillo library in this program
using namespace arma;
using namespace std;

int main()
{

    //Declare local variables
    int i, j, k, n;
    double time1, time2, time3, time4, time5, time6;

    n =100;

    // Make and fill matrix A with zeros, six times
    // Make and fill matrices B and C with random numbers, six times
    mat A_mat1(n,n);
    mat B_mat1(n,n);
    mat C_mat1(n,n);
    A_mat1.fill(0);
    B_mat1.randu();
    C_mat1.randu();

    mat A_mat2(n,n);
    mat B_mat2(n,n);
    mat C_mat2(n,n);
    A_mat2.fill(0);
    B_mat2.randu();
    C_mat2.randu();

    mat A_mat3(n,n);
    mat B_mat3(n,n);
    mat C_mat3(n,n);
    A_mat3.fill(0);
    B_mat3.randu();
    C_mat3.randu();

    mat A_mat4(n,n);
    mat B_mat4(n,n);
    mat C_mat4(n,n);
    A_mat4.fill(0);
    B_mat4.randu();
    C_mat4.randu();

    mat A_mat5(n,n);
    mat B_mat5(n,n);
    mat C_mat5(n,n);
    A_mat5.fill(0);
    B_mat5.randu();
    C_mat5.randu();

    mat A_mat6(n,n);
    mat B_mat6(n,n);
    mat C_mat6(n,n);
    A_mat6.fill(0);
    B_mat6.randu();
    C_mat6.randu();


    // Start clock to compute spend time
    clock_t start1, finish1;
    start1 = clock();

    // Trippel for-loop to multiply B and C
    // We are doing the same muliplication 6 times, but with different order of the indexes i, j and k
    for(i=0; i < n; i++){
        for(j=0; j < n; j++){
            for(k=0; k < n; k++){
                A_mat1(i,j) += B_mat1(i,k)*C_mat1(k,j);
            }
        }
    }
    // Stop the clock and estimate the spend time
    finish1 = clock();
    time1 = ((finish1 - start1)/((double) CLOCKS_PER_SEC));

    clock_t start2, finish2;
    start2 = clock();
    for(k=0; k < n; k++){
        for(i=0; i < n; i++){
            for(j=0; j < n; j++){
                A_mat2(i,j) += B_mat2(i,k)*C_mat2(k,j);
            }
        }
    }
    finish2 = clock();
    time2 = ((finish2 - start2)/((double) CLOCKS_PER_SEC));


    clock_t start3, finish3;
    start3 = clock();
    for(j=0; j < n; j++){
        for(k=0; k < n; k++){
            for(i=0; i < n; i++){
                A_mat3(i,j) += B_mat3(i,k)*C_mat3(k,j);
            }
        }
    }
    finish3 = clock();
    time3 = ((finish3 - start3)/((double) CLOCKS_PER_SEC));

    clock_t start4, finish4;
    start4 = clock();
    for(k=0; k < n; k++){
        for(j=0; j < n; j++){
            for(i=0; i < n; i++){
               A_mat4(i,j) += B_mat4(i,k)*C_mat4(k,j);
            }
        }
    }
    finish4 = clock();
    time4 = ((finish4 - start4)/((double) CLOCKS_PER_SEC));

    clock_t start5, finish5;
    start5 = clock();
    for(j=0; j < n; j++){
        for(i=0; i < n; i++){
            for(k=0; k < n; k++){
                A_mat5(i,j) += B_mat5(i,k)*C_mat5(k,j);
            }
        }
    }
    finish5 = clock();
    time5 = ((finish5 - start5)/((double) CLOCKS_PER_SEC));

    clock_t start6, finish6;
    start6 = clock();
    for(i=0; i < n; i++){
        for(k=0; k < n; k++){
            for(j=0; j < n; j++){
                A_mat6(i,j) += B_mat6(i,k)*C_mat6(k,j);
            }
        }
    }
    finish6 = clock();
    time6 = ((finish6 - start6)/((double) CLOCKS_PER_SEC));

    // Write out the spend time for all the six different orders of i, j and k in the for-loops
    cout << "n-value: " << n << "   Time matrix-multiplication: " << time1 << "s ijk   "<< time2 << "s kij    " << time3 << "s jki   "<< time4 << "s kji    " << time5 << "s jik    " << time6 << "s ikj"<< endl;

    return 0;
}


// Not a part of this program

// If using lib.h, have to download and include lib.h
/*
double **a = new double *[n];
for(i=0; i < n; i++){
    a[i] = new double[n];
}

double **b = new double *[n];
for(i=0; i < n; i++){
    b[i] = new double[n];
}

double **c = new double *[n];
for(i=0; i < n; i++){
    c[i] = new double[n];
}


for(i=0; i < n; i++){
    for(j=0; j < n; j++){
        a[i][j] = rand();
    }
}

for(i=0; i < n; i++){
    for(j=0; j < n; j++){
       b[i][j] = rand();
    }
}

for(i=0; i < n; i++){
    for(j=0; j < n; j++){
        c[i][j] = ran0;
    }
}

cout << a[2][1]<< endl;
cout << b[2][1]<< endl;
cout << c[2][1]<< endl;
*/


