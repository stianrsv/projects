clf;
f = load('Project1_b_10.dat');

plot(f(:,1), f(:,2), 'r-', f(:,1), f(:,3), 'bo');

legend('analytisk','numerisk');
title('Oppgave b, n = 10')
xlabel('x');
ylabel('f(x)');
%print -dpng project1_b_10.png ;