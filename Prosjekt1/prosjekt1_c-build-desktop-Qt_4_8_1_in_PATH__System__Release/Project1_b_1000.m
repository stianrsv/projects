f = load('Project1_b_1000.dat');

figure;
plot(f(:,1), f(:,2), 'r-', f(:,1), f(:,3), 'bo');

legend('analytisk','numerisk');
title('Oppgave b, n = 1000');
xlabel('x');
ylabel('f(x)');
print -dpng project1_b_1000.png ;