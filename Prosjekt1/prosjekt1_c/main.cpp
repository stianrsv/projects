// Project1 b
/* This program compute a numerical and an analytical solution for our given problem,
  and the computed values are written to another file to make a plot of the results
*/

#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;

int main()
{

    //Declare variables
    int n, i;
    n = 10;

    double h, h2f;
    h = 1.0/(n+1);
    h2f = pow(h,2.0);

    // Makes vectors from 0 to n+1 with dynamic memory allocation
    double *a = new double[n+2];
    double *b = new double[n+2];
    double *c = new double[n+2];
    double *f = new double[n+2];
    double *v = new double[n+2];
    double *f_analytic = new double[n+2];


    // Open a writeabel file in the build directory for this program
    fstream outfile;
    outfile.open("Project1_b_10.dat", ios::out);

    // Fills up the three vector in our matrix with values
    for(i=0; i <= n+1; i++){
        a[i] = -1;
        b[i] = 2;
        c[i] = -1;
        f[i] = h2f*100*exp(-10*i*h);
    }

    // Forward substitution with our own algorithm
    for(i=2; i <= n; i++){
        b[i] = b[i]-(a[i]*c[i-1])/b[i-1];
        f[i] = f[i]-(a[i]*f[i-1])/b[i-1];
    }

    // Set initial conditions
    v[0] = 0;
    f_analytic[0] = 0;


    // Backward substitution with our own algorithm
    v[n] = f[n]/b[n];

    for(i=n-1; i >= 1; i--){
        v[i] = (f[i]+v[i+1])/b[i];
    }

    // Compute the exact analytical solution to our differensial equation
    for(i=1; i <=n+2; i++){
        f_analytic[i] = 1 -(1-exp(-10))*i*h - exp(-10*i*h);
        cout << "analytic: " << f_analytic[i-1] << " numeric: "<< v[i-1] << endl;
        outfile << (i-1)*h << ' ' << f_analytic[i-1] << ' ' << v[i-1] <<endl;
    }
    // Closing the writeabel file in the build directory for this program
    outfile.close();

    return 0;
}
