// Exercise a
// Implement Jacobis method

#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include "armadillo"

using namespace std;
using namespace arma;

// Find largest nondiagonal element in a given matrix "a"
double maxOffDiagonal(const mat a, int* k, int* l, double max, int n){
    max = 0.0;
    for(int i = 0; i < n; i++){
        for(int j = i+1; j < n; j++){
            if(fabs(a(i,j)) > max){
                max = fabs(a(i,j));
                *l = i;
                *k = j;
            }
        }
    }
    return max;
}


int main()
{
    int n, i, k, l, g;
    double rho_max, rho_min, h, s, c, t, tau, max, max_error;

    n = 100 ;
    max_error = pow(10,-10);

// Setting up vectors and matrices we are going to use in the program
    vec rho(n);
    vec V(n);
    vec d(n);
    vec e_vec(n-1);
    mat a = zeros(n,n);

// Define elements for our main matrix "a"
    for(i = 0; i < n; i++){
        rho_min = 0;
        rho_max = 5.0;// "random" number lower than infinity
        h = ((rho_max - rho_min)/(n+1));
        rho(i) = rho_min + ((i+1)*h);
        V(i) = pow(rho(i), 2);
        d(i) = (2.0/pow(h,2)) + V(i);
    }

    e_vec.fill(-1.0/pow(h,2));
    a.diag(0) = d;
    a.diag(-1) = e_vec;
    a.diag(1) = e_vec;

    g = 0;
    max = 1.0;

// Minimize differense between matrix "a" and transformed matrix
    while(max > max_error){
        g++;
        max =  maxOffDiagonal(a, &k, &l, max, n);

        tau = (a(l,l)-a(k,k))/(2.0*a(k,l));

        if (tau > 0){
            t = 1.0/(tau + sqrt(1+pow(tau,2)));
        }
        else{
            t = -1.0/(-tau + sqrt(1+pow(tau,2)));
        }

// Trigonometric relations for the Jacobi method
        c = 1.0/(sqrt(1.0 + pow(t,2)));
        s = t*c;

        double aik, akk, all;

// The Jacobi method
        for(int i = 0; i < n; i++){
            if((i!=k && i!=l)){
                aik = a(i,k);
                a(i,k) = a(i,k)*c - a(i,l)*s;
                a(i,l) = a(i,l)*c  + aik*s;
                a(k,i) = a(i,k);
                a(l,i) = a(i,l);
            }
        }

        akk = a(k,k);
        all = a(l,l);

        a(k,k) = a(k,k)*pow(c,2) - 2.0*a(k,l)*c*s + a(l,l)*pow(s,2);
        a(l,l) = a(l,l)*pow(c,2) + 2.0*a(k,l)*c*s + akk*pow(s,2);
        a(k,l) = (akk - all)*c*s + a(k,l)*(pow(c,2) - pow(s,2));
        a(l,k) = a(k,l);
    }

// Find a vector with sorted eigenvalues
    vec A = sort(a.diag(0));
    cout << A << endl;

    return 0;
}

