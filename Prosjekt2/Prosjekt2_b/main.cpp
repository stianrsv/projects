// Exercise b
// Using Jacobis algorith to find eigenvalues for the singel electron system
// and compute effectiviness of Jacobis algorith and Armadillos solver

#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include "armadillo"
#include "time.h"

using namespace std;
using namespace arma;

// Find largest nondiagonal element in a given matrix "a"
double maxOffDiagonal(const mat a, int* k, int* l, double max, int n){
    max = 0.0;
    for(int i = 0; i < n; i++){
        for(int j = i+1; j < n; j++){
            if(fabs(a(i,j)) > max){
                max = fabs(a(i,j));
                *l = i;
                *k = j;
            }
        }
    }
    return max;
}


int main()
{
    int j;

    double time_jacobi, time_arma;

// Open writeable files
    fstream outfile1, outfile2;
    outfile1.open("number_of_transformations.dat", ios::out);
    outfile2.open("time_solving_eigenvalue.dat", ios::out);

    for (j=1; j <= 60; j++){

        int n, i, k, l, g, M;
        double rho_max, rho_min, h, s, c, t, tau, max, max_error;

// Define a varying value of "n"
        M = 5*j;
        n = M;

        max_error = pow(10,-10);

// Setting up vectors and matrices we are going to use in the program
        vec rho(n);
        vec V(n);
        vec d(n);
        vec e_vec(n-1);
        mat a = zeros(n,n);
        mat b(n,n);

// Define elements for our main matrix "a"
        for(i = 0; i < n; i++){
            rho_min = 0.0;
            rho_max = 5.0;// "random" number lower than infinity
            h = ((rho_max - rho_min)/(n+1));
            rho(i) = rho_min + ((i+1)*h);
            V(i) = pow(rho(i), 2);
            d(i) = (2.0/pow(h,2)) + V(i);
        }

        e_vec.fill(-1.0/pow(h,2));
        a.diag(0) = d;
        a.diag(-1) = e_vec;
        a.diag(1) = e_vec;

        b = a;

        g = 0;
        max = 1.0;

// Start clock to find effectivity of the Jacobi algorithm
        clock_t start1, finish1;
        start1 = clock();

// Minimize differense between matrix "a" and transformed matrix
        while(max > max_error){
            g++;
            max =  maxOffDiagonal(a, &k, &l, max, n);

            tau = (a(l,l)-a(k,k))/(2.0*a(k,l));

            if (tau > 0){
                t = 1.0/(tau + sqrt(1+pow(tau,2)));
            }
            else{
                t = -1.0/(-tau + sqrt(1+pow(tau,2)));
            }

// Trigonometric relations for the Jacobi method
            c = 1.0/(sqrt(1.0 + pow(t,2)));
            s = t*c;

            double aik, akk, all;

// The Jacobi method
            for(int i = 0; i < n; i++){
                if((i!=k && i!=l)){
                    aik = a(i,k);
                    a(i,k) = a(i,k)*c - a(i,l)*s;
                    a(i,l) = a(i,l)*c  + aik*s;
                    a(k,i) = a(i,k);
                    a(l,i) = a(i,l);
                }
            }

            akk = a(k,k);
            all = a(l,l);

            a(k,k) = a(k,k)*pow(c,2) - 2.0*a(k,l)*c*s + a(l,l)*pow(s,2);
            a(l,l) = a(l,l)*pow(c,2) + 2.0*a(k,l)*c*s + akk*pow(s,2);
            a(k,l) = (akk - all)*c*s + a(k,l)*(pow(c,2) - pow(s,2));
            a(l,k) = a(k,l);
        }

// Stop the clock
        finish1 = clock();
        time_jacobi = ((finish1 - start1))/((double) CLOCKS_PER_SEC);

        vec A = sort(a.diag(0));

// Setting up vector and matrix to solve the eigenvalueproblem with "Armadillo"
        mat eigenvector;
        vec eigenvalue;

        clock_t start2, finish2;
        start2 = clock();

// Armadillo method for solving eigenvalue problems
        eig_sym(eigenvalue, eigenvector, b);

        finish2 = clock();
        time_arma = ((finish2 - start2))/((double) CLOCKS_PER_SEC);

        vec B = sort(eigenvalue);
        //cout << B;

        //cout << "n-value: " << n << "   " << "Time jacobi_method: " <<  time_jacobi << "   " << "Time Armadillo: " << time_arma << endl;

        //cout << "n = " << n << endl;
        //cout << "rho_max = " << rho_max << endl;
        //cout << "Sorted eigenvalues: " << endl << A << endl;
        cout << "Number of transformations, n = " << n << "  " << " : " << g << endl;

// Write results to files
        outfile1 << n << " " << g << endl;
        outfile2 << n << " " << time_jacobi << " " << time_arma << endl;
    }

// Closing writeable files
    outfile1.close();
    outfile2.close();
    return 0;
}



