// Exercise c, part 2
// Test the convergence of our estimated eigenvalues (in ground state) for incresing precision


#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include "armadillo"

using namespace std;
using namespace arma;

// Find largest nondiagonal element in a given matrix "a"
double maxOffDiagonal(const mat a, int* k, int* l, double max, int n){
    max = 0.0;
    for(int i = 0; i < n; i++){
        for(int j = i+1; j < n; j++){
            if(fabs(a(i,j)) > max){
                max = fabs(a(i,j));
                *l = i;
                *k = j;
            }
        }
    }
    return max;
}

int main()
{

// Open writeable file
    fstream outfile2;
    outfile2.open("eigenvalues_dependence_n.dat", ios::out);

    int p;

    for(p=0; p < 5; p++){

        double wr, rho_max;

// Set up vector with different values for the frequency
        vec wr_vec = zeros(5);
        wr_vec(0) = 0.01;
        wr_vec(1) = 0.25;
        wr_vec(2) = 0.5;
        wr_vec(3) = 1.0;
        wr_vec(4) = 5.0;
        wr = wr_vec(p);

// Varying value for the width of harmonic potential
        rho_max = 35.0 - 7*p;

        int j;
        for (j=0; j < 5; j++){

            int n, i, k, l, g;
            double rho_min, h, s, c, t, tau, max, max_error, lowest_eigenvalue;

            n = 10*pow(2,(j+1));

            max_error = pow(10,-10);

// Setting up vectors and matrices we are going to use in the program
            vec rho(n);
            vec V1(n);
            vec d(n);
            vec e_vec(n-1);
            mat a = zeros(n,n);

// Define elements for our main matrix "a"
            for(i = 0; i < n; i++){
                rho_min = 0.0;
                h = ((rho_max - rho_min)/(n+1));
                rho(i) = rho_min + ((i+1)*h);
                V1(i) = (pow(wr,2))*(pow(rho(i), 2)) + (1.0/rho(i));
                d(i) = (2.0/pow(h,2)) + V1(i);
            }

            e_vec.fill(-1.0/pow(h,2));
            a.diag(0) = d;
            a.diag(-1) = e_vec;
            a.diag(1) = e_vec;

            g = 0;
            max = 1.0;

// Minimize differense between matrix "a" and transformed matrix
            while(max > max_error){
                g++;
                max =  maxOffDiagonal(a, &k, &l, max, n);

                tau = (a(l,l)-a(k,k))/(2.0*a(k,l));

                if (tau > 0){
                    t = 1.0/(tau + sqrt(1+pow(tau,2)));
                }
                else{
                    t = -1.0/(-tau + sqrt(1+pow(tau,2)));
                }

// Trigonometric relations for the Jacobi method
                c = 1.0/(sqrt(1.0 + pow(t,2)));
                s = t*c;

                double aik, akk, all;

// The Jacobi method
                for(int i = 0; i < n; i++){
                    if((i!=k && i!=l)){
                        aik = a(i,k);
                        a(i,k) = a(i,k)*c - a(i,l)*s;
                        a(i,l) = a(i,l)*c  + aik*s;
                        a(k,i) = a(i,k);
                        a(l,i) = a(i,l);
                    }
                }
                akk = a(k,k);
                all = a(l,l);
                a(k,k) = a(k,k)*pow(c,2) - 2.0*a(k,l)*c*s + a(l,l)*pow(s,2);
                a(l,l) = a(l,l)*pow(c,2) + 2.0*a(k,l)*c*s + akk*pow(s,2);
                a(k,l) = (akk - all)*c*s + a(k,l)*(pow(c,2) - pow(s,2));
                a(l,k) = a(k,l);
            }
            vec eigenvalues = sort(a.diag(0));
            lowest_eigenvalue = eigenvalues.min();

            //cout << wr_vec << endl;
            //cout << eigenvalues;
            //cout << lowest_eigenvalue << endl;
            //cout << "n = " << n << "  " << "h = " << h << "  " << "rho_max = " << rho_max << "  " << "wr = " << wr << "  " << "Lowest_eigenvalue = " << lowest_eigenvalue << endl;
            //outfile1 << wr << " " << lowest_eigenvalue << endl;


// Write results to files
            if(n == 20){
                outfile2 << lowest_eigenvalue << " ";
            }
            if(n == 40){
                outfile2 << lowest_eigenvalue << " ";
            }
            if(n == 80){
                outfile2 << lowest_eigenvalue << " ";
            }
            if(n == 160){
                outfile2 << lowest_eigenvalue << " ";
            }
            if(n == 320){
                outfile2 << lowest_eigenvalue << endl;
            }
        }
    }
// Closing writeable file
    outfile2.close();
    return 0;
}




