// Exercise d
// Pics out the eigenvector corresponding the lowest eigenvalues for
// different strength of the harmonic potential, and computes the normalized
// eigenvector squared, in other words the wavefuntion for the two electron system
// for diffenrent strength of the harmonic potential


#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>
#include "armadillo"

using namespace std;
using namespace arma;

// Find largest nondiagonal element in a given matrix "a"
double maxOffDiagonal(const mat a, int* k, int* l, double max, int n){
    max = 0.0;
    for(int i = 0; i < n; i++){
        for(int j = i+1; j < n; j++){
            if(fabs(a(i,j)) > max){
                max = fabs(a(i,j));
                *l = i;
                *k = j;
            }
        }
    }
    return max;
}

int main()
{

// Open writeable files
    fstream outfile1, outfile2, outfile3, outfile4, outfile5;

    outfile1.open("eigenvector_wr_0.01.dat", ios::out);
    outfile2.open("eigenvector_wr_0.25.dat", ios::out);
    outfile3.open("eigenvector_wr_0.5.dat", ios::out);
    outfile4.open("eigenvector_wr_1.0.dat", ios::out);
    outfile5.open("eigenvector_wr_5.0.dat", ios::out);

    int j;
    for (j=0; j < 5; j++){

        int n, i, k, l, g;
        double rho_min, h, s, c, t, tau, max, max_error, lowest_eigenvalue, rik, ril;

        n = 200;

        max_error = pow(10,-10);

// Setting up vectors and matrices we are going to use in the program
        mat ID = eye(n,n); // identity matrix
        vec rho(n);
        vec V1(n);
        vec d(n);
        vec e_vec(n-1);
        mat a = zeros(n,n);

        double wr, rho_max;

// Set up vector with different values for the frequency
        vec wr_vec = zeros(5);
        wr_vec(0) = 0.01;
        wr_vec(1) = 0.25;
        wr_vec(2) = 0.5;
        wr_vec(3) = 1.0;
        wr_vec(4) = 5.0;
        wr = wr_vec(j);

// Define elements for our main matrix "a"
        for(i = 0; i < n; i++){
            rho_min = 0.0;
// Varying value for the width of harmonic potential
            rho_max = 35.0-7*j;
            h = ((rho_max - rho_min)/(n+1));
            rho(i) = rho_min + ((i+1)*h);
            V1(i) = (pow(wr,2))*(pow(rho(i), 2)) + (1.0/rho(i));
            d(i) = (2.0/pow(h,2)) + V1(i);
        }

        e_vec.fill(-1.0/pow(h,2));
        a.diag(0) = d;
        a.diag(-1) = e_vec;
        a.diag(1) = e_vec;

        g = 0;
        max = 1.0;

// Minimize differense between matrix "a" and transformed matrix
        while(max > max_error){
            g++;
            max =  maxOffDiagonal(a, &k, &l, max, n);

            tau = (a(l,l)-a(k,k))/(2.0*a(k,l));

            if (tau > 0){
                t = 1.0/(tau + sqrt(1+pow(tau,2)));
            }
            else{
                t = -1.0/(-tau + sqrt(1+pow(tau,2)));
            }

// Trigonometric relations for the Jacobi method
            c = 1.0/(sqrt(1.0 + pow(t,2)));
            s = t*c;

            double aik, akk, all;

// The Jacobi method
            for(int i = 0; i < n; i++){
                if((i!=k && i!=l)){
                    aik = a(i,k);
                    a(i,k) = a(i,k)*c - a(i,l)*s;
                    a(i,l) = a(i,l)*c  + aik*s;
                    a(k,i) = a(i,k);
                    a(l,i) = a(i,l);
                }

// Identity matrix "ID" transformes into a matrix with the corresponding eigenvectors to matrix "a"
                rik = ID(i,k);
                ril = ID(i,l);
                ID(i,k) = c*rik - s*ril;
                ID(i,l) = c*ril + s*rik;
            }
            akk = a(k,k);
            all = a(l,l);
            a(k,k) = a(k,k)*pow(c,2) - 2.0*a(k,l)*c*s + a(l,l)*pow(s,2);
            a(l,l) = a(l,l)*pow(c,2) + 2.0*a(k,l)*c*s + akk*pow(s,2);
            a(k,l) = (akk - all)*c*s + a(k,l)*(pow(c,2) - pow(s,2));
            a(l,k) = a(k,l);
        }

        vec eigenvalues = a.diag(0);

// Pics out the colum with the eigenvector corresponding to the lowest eigenvalue
        uword  index;
        int min_val = eigenvalues.min(index);
        vec eigenvector = zeros(n);

// Makes a new vector with only one eigenvector corresponding to the lowest eigenvalue
        for(i = 0;i<n;i++){
            eigenvector(i) = ID(i,index);
        }


// Normalize the eigenvector corresponding to the lowest eigenvalue of the system
        double normalized = 0.0;
        for (i=0;i<n;i++){
            normalized += pow(eigenvector(i), 2);
        }
        eigenvector /= sqrt(h*normalized);

        //cout << eigenvector << endl;


// Write results to files
        for (i=0; i<n; i++){
            if (wr == 0.01){
                outfile1 << rho(i) << " " << pow(eigenvector(i),2) << endl;
            }
            if (wr == 0.25){
                outfile2 << rho(i) << " " << pow(eigenvector(i),2) << endl;
            }
            if (wr == 0.5){
                outfile3 << rho(i) << " " << pow(eigenvector(i),2) << endl;
            }
            if (wr == 1.0){
                outfile4 << rho(i) << " " << pow(eigenvector(i),2) << endl;
            }
            if (wr == 5.0){
                outfile5 << rho(i) << " " << pow(eigenvector(i),2) << endl;
            }
        }
    }
// Closing writeable files
    outfile1.close();
    outfile2.close();
    outfile3.close();
    outfile4.close();
    outfile5.close();
    return 0;

}










