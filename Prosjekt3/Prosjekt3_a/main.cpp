// Program that compute the hole solarsystem witk RK4 without using classes


#include <iostream>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <fstream>
#include <string>
#include <stdlib.h>

const double pi = 4*atan(1.0);
const double G = 4*pow(pi,2);

using namespace std;
using namespace arma;

int n = 10; // number of planets
int N = 2500000; //2 500 000 number of steps
double dt = 0.0001; // length time steps


// Makes a vector with [vx,vy,ax,ay, ...], using previous vector [x,y,vx,vy]
vec f(vec q, vec M){
    double r, f_force, x, y;
    int i, j;
    vec f_values = zeros(4*n);
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            x = q(4*i) - q(4*j);
            y = q(4*i+1) - q(4*j+1);
            r = sqrt(x*x + y*y);
            if (i != j){
                f_force = -G*M(j)/(pow(r,3));
                f_values(4*i+2) += f_force*x;
                f_values(4*i+3) += f_force*y;
            }
        }
    }
    for(i=0; i < n;i++){
        f_values(4*i) = q(4*i+2);
        f_values(4*i+1) = q(4*i+3);
    }
    return f_values;
}

//Runge-Kutta4 method to solve system of differential equations
void RK4(vec &q, vec M){
    vec k1 = zeros(4*n);
    vec k2 = zeros(4*n);
    vec k3 = zeros(4*n);
    vec k4 = zeros(4*n);

    k1 = f(q, M)*dt;
    k2 = f(q + 0.5*k1, M)*dt;
    k3 = f(q + 0.5*k2, M)*dt;
    k4 = f(q + k3, M)*dt;

    q += (1.0/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);

}

int main()
{
    fstream outfile1, outfile2, outfile3, outfile4, outfile5, outfile6, outfile7, outfile8, outfile9, outfile10;
    outfile1.open("sun.dat", ios::out);
    outfile2.open("earth.dat", ios::out);
    outfile3.open("jupiter.dat", ios::out);
    outfile4.open("mars.dat", ios::out);
    outfile5.open("venus.dat", ios::out);
    outfile6.open("saturn.dat", ios::out);
    outfile7.open("mercury.dat", ios::out);
    outfile8.open("uranus.dat", ios::out);
    outfile9.open("neptun.dat", ios::out);
    outfile10.open("pluto.dat", ios::out);

    int k;

    vec q = zeros(4*n);
    vec M = zeros(n);
    vec initial_values_vec(4*n);
    vec mass_planets_vec(n);

    // Open outfiles with initialvalues and masses
    string line;
    ifstream initial_values, mass_planets;
    initial_values.open("initial_values_planets.txt", ios::in);
    mass_planets.open("rel_mass_planets.txt", ios::in);


    // Reading outfile with initial values
    k = 0;
    if (initial_values.is_open()){
        for(k=0;k < 4*n; k++){
            getline(initial_values,line);
            double initial_values_from_file = atof(line.c_str());
            //cout   << k++ << " " <<  initial_values_from_file << endl;
            initial_values_vec(k) = initial_values_from_file;
        }
        initial_values.close();
    }


    //Reading outfile with masses
    k = 0;
    if (mass_planets.is_open()){
        for(k=0;k<n;k++){
            getline(mass_planets,line);
            double mass_planets_from_file = atof(line.c_str());
            //cout   << mass_planets_from_file << endl;
            mass_planets_vec(k) = mass_planets_from_file;

        }
        mass_planets.close();
    }

    for(k=0; k < 4*n ;k++){
        //cout << k << " " << initial_values_vec(k) << endl;
    }
    for(k=0; k<n; k++){
        //cout << mass_planets_vec(k) << endl;
    }


    // Fills a vector with initialvalues
    for(k=0; k < 4*n; k++){
        q(k) = initial_values_vec(k);
        //cout << q(k) << endl;
    }


    // Fills a vector with masses
    for(k=0; k < n; k++){
        M(k) = mass_planets_vec(k);
        //cout << M(k) << endl;
    }


    // Running RK4 N steps
    for(k=0; k < N; k++){
        RK4(q, M);

        outfile1 << q(0) << " " << q(1) << endl;
        outfile2 << q(4) << " " << q(5) << endl;
        outfile3 << q(8) << " " << q(9) << endl;
        outfile4 << q(12) << " " << q(13) << endl;
        outfile5 << q(16) << " " << q(17) << endl;
        outfile6 << q(20) << " " << q(21) << endl;
        outfile7 << q(24) << " " << q(25) << endl;
        outfile8 << q(28) << " " << q(29) << endl;
        outfile9 << q(32) << " " << q(33) << endl;
        outfile10 << q(36) << " " << q(37) << endl;
    }

    outfile1.close();
    outfile2.close();
    outfile3.close();
    outfile4.close();
    outfile4.close();
    outfile5.close();
    outfile6.close();
    outfile7.close();
    outfile8.close();
    outfile9.close();
    outfile10.close();

    return 0;

}
