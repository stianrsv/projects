// This program run the RK4 as class for sun-earth system, and test energy and angular conservation


#include <iostream>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;
using namespace arma;

const double pi = 4*atan(1.0);
const double G = 4*pow(pi,2);


// Class to set values corresponding different planets
class Planet {
public:
    double x, y, vx, vy, M;
    Planet(double a, double b, double c, double d, double e);
    Planet() {}
};

Planet::Planet(double a, double b, double c, double d, double e){
    x = a;
    y = b;
    vx = c;
    vy = d;
    M = e;
}

// Class to use RK4 to solve the differential equations
class Solver {
public:
    int n, N;
    double dt;
    vec q;
    void q_func();
    void initialize_planet(Planet name);
    Planet planetlist[200]; // random number higher than number of planets
    Solver (int N_value, double dt_value);
    vec f(vec l);
    void RK4();
};

// Function to add planets to the solarsystem
void Solver::initialize_planet(Planet name){
    planetlist[n] = name;
    n++;
}

// Constructor
Solver::Solver(int N_value, double dt_value){
    n = 0;
    N = N_value;
    dt = dt_value;
}

// Function to initialize initialvalues
void Solver::q_func(){
    q = zeros(4*n);
    for(int i=0; i<n; i++){
        q[4*i] = planetlist[i].x;
        q[4*i+1] = planetlist[i].y;
        q[4*i+2] = planetlist[i].vx;
        q[4*i+3] = planetlist[i].vy;
    }
}


// Function to call in RK4
vec Solver::f(vec q_new){
    double r, f_force, x, y;
    int i, j;
    vec f_values = zeros(4*n);
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            x = q_new(4*i) - q_new(4*j);
            y = q_new(4*i+1) - q_new(4*j+1);
            r = sqrt(x*x + y*y);
            if (i != j){
                f_force = -G*planetlist[j].M/(pow(r,3));
                f_values(4*i+2) += f_force*x;  // M(i) = planetlist[i].M
                f_values(4*i+3) += f_force*y;
            }
        }
    }
    for(i=0; i < n; i++){
        f_values(4*i) = q_new(4*i+2);
        f_values(4*i+1) = q_new(4*i+3);
    }
    return f_values;
}

// RK4 method
void Solver:: RK4(){
    vec k1 = zeros(4*n);
    vec k2 = zeros(4*n);
    vec k3 = zeros(4*n);
    vec k4 = zeros(4*n);

    k1 = f(q)*dt;
    k2 = f(q + 0.5*k1)*dt;
    k3 = f(q + 0.5*k2)*dt;
    k4 = f(q + k3)*dt;

    q += (1.0/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
}


int main(){

    int N, k;
    double dt, e_kin1, e_kin2, e_pot1, e_pot2, r1, r2, v, v1, v2, r, tetta, angular1, angular2;

    N = 250000; // 2000 collaps and shoots out
    dt = 0.0001;

    cout << "dt = " << dt << endl << endl;
    fstream outfile1, outfile2;
    outfile1.open("sun.dat", ios::out);
    outfile2.open("earth.dat", ios::out);


    // Initial_values for all planets(x, y, vx, vy, M)
    Planet sun(0.0, 0.0, 0.0, 0.0, 1.0);
    Planet earth(0.0, 1.0, 2*pi, 0.0, 4*pow(10,-6));

    // Call Solver
    Solver Solarsystem = Solver(N, dt); // give N and dt;

    // Adding planets to Solarsystem
    Solarsystem.initialize_planet(sun);
    Solarsystem.initialize_planet(earth);

    // Call funtions in class Solver
    Solarsystem.q_func();
    Solarsystem.RK4();

    v1 = sqrt(pow(Solarsystem.q(2),2)+pow(Solarsystem.q(3),2));
    v2 = sqrt(pow(Solarsystem.q(6),2)+pow(Solarsystem.q(7),2));
    v = fabs(v1-v2);

    r1 = sqrt(pow(Solarsystem.q(0),2)+pow(Solarsystem.q(1),2));
    r2 = sqrt(pow(Solarsystem.q(4),2)+pow(Solarsystem.q(5),2));
    r = fabs(r1-r2);

    tetta = pi/2.0;

    e_kin1 = 0.5*earth.M*pow(v,2);
    e_pot1 = (G*sun.M*earth.M)/pow(r,2);
    angular1 = r*v*sin(tetta); // antar i hele banen at tetta = pi/2 pga. initialbetingelser

    // Does the RK4 method N times on initialvalues and write positions to file
    for(k=0; k < N; k++){
        Solarsystem.RK4();
        outfile1 << Solarsystem.q(0) << " " << Solarsystem.q(1) << endl;
        outfile2 << Solarsystem.q(4) << " " << Solarsystem.q(5) << endl;
    }

    v1 = sqrt(pow(Solarsystem.q(2),2)+pow(Solarsystem.q(3),2));
    v2 = sqrt(pow(Solarsystem.q(6),2)+pow(Solarsystem.q(7),2));
    v = fabs(v1-v2);

    r1 = sqrt(pow(Solarsystem.q(0),2)+pow(Solarsystem.q(1),2));
    r2 = sqrt(pow(Solarsystem.q(4),2)+pow(Solarsystem.q(5),2));
    r = fabs(r1-r2);

    e_kin2 = 0.5*earth.M*pow(v,2);
    e_pot2 = (G*sun.M*earth.M)/pow(r,2);
    angular2 = r*v;//*sin(tetta);

    cout << "E_kin_start: " << e_kin1 << endl;
    cout << "E_kin after " << N << " " << "steps with RK4: " << e_kin2 << endl;
    cout << "Error in e_kin: " << (fabs(e_kin1-e_kin2)) << endl << endl;

    cout << "E_pot_start: " << e_pot1 << endl;
    cout << "E_pot after " << N << " " << "steps with RK4: " << e_pot2 << endl;
    cout << "Error in e_pot: " << (fabs(e_pot1-e_pot2)) << endl << endl;

    cout << "Angular momentum start: " << angular1 << endl;
    cout << "Angular momentum after " << N << " " << "steps with RK4: " << angular2 << endl;
    cout << "Error in angular momentum: " << (fabs(angular1-angular2)) << endl << endl;

    outfile1.close();
    outfile2.close();

    return 0;
}





