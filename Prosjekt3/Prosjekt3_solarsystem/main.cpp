// This program computes the hole Solarsystem using RK4 and classes


#include <iostream>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;
using namespace arma;

const double pi = 4*atan(1.0);
const double G = 4*pow(pi,2);


// Class to set values corresponding different planets
class Planet {
public:
    double x, y, vx, vy, M;
    Planet(double a, double b, double c, double d, double e);
    Planet() {}
};

Planet::Planet(double a, double b, double c, double d, double e){
    x = a;
    y = b;
    vx = c;
    vy = d;
    M = e;
}

// Class to use RK4 to solve the differential equations
class Solver {
public:
    int n, N;
    double dt;
    vec q;
    void q_func();
    void initialize_planet(Planet name);
    Planet planetlist[200]; // random number higher than number of planets
    Solver (int N_value, double dt_value);
    vec f(vec l);
    void RK4();
};

// Function to add planets to the solarsystem
void Solver::initialize_planet(Planet name){
    planetlist[n] = name;
    n++;
}

// Constructor
Solver::Solver(int N_value, double dt_value){
    n = 0;
    N = N_value;
    dt = dt_value;
}

// Function to initialize initialvalues
void Solver::q_func(){
    q = zeros(4*n);
    for(int i=0; i<n; i++){
        q[4*i] = planetlist[i].x;
        q[4*i+1] = planetlist[i].y;
        q[4*i+2] = planetlist[i].vx;
        q[4*i+3] = planetlist[i].vy;
    }
}


// Function to call in RK4
vec Solver::f(vec q_new){
    double r, f_force, x, y;
    int i, j;
    vec f_values = zeros(4*n);
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            x = q_new(4*i) - q_new(4*j);
            y = q_new(4*i+1) - q_new(4*j+1);
            r = sqrt(x*x + y*y);
            if (i != j){
                f_force = -G*planetlist[j].M/(pow(r,3));
                f_values(4*i+2) += f_force*x;  // M(i) = planetlist[i].M
                f_values(4*i+3) += f_force*y;
            }
        }
    }
    for(i=0; i < n; i++){
        f_values(4*i) = q_new(4*i+2);
        f_values(4*i+1) = q_new(4*i+3);
    }
    return f_values;
}

// RK4 method
void Solver:: RK4(){
    vec k1 = zeros(4*n);
    vec k2 = zeros(4*n);
    vec k3 = zeros(4*n);
    vec k4 = zeros(4*n);

    k1 = f(q)*dt;
    k2 = f(q + 0.5*k1)*dt;
    k3 = f(q + 0.5*k2)*dt;
    k4 = f(q + k3)*dt;

    q += (1.0/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
}

int main(){

    int N, k;
    double dt;

    N = 250000; // Number of iterations
    dt = 0.001; // length of steps in RK4


    fstream outfile1, outfile2, outfile3, outfile4, outfile5, outfile6, outfile7, outfile8, outfile9, outfile10;
    outfile1.open("sun.dat", ios::out);
    outfile2.open("earth.dat", ios::out);
    outfile3.open("jupiter.dat", ios::out);
    outfile4.open("mars.dat", ios::out);
    outfile5.open("venus.dat", ios::out);
    outfile6.open("saturn.dat", ios::out);
    outfile7.open("mercury.dat", ios::out);
    outfile8.open("uranus.dat", ios::out);
    outfile9.open("neptun.dat", ios::out);
    outfile10.open("pluto.dat", ios::out);


    // Initial_values for all planets(x, y, vx, vy, M)
    Planet sun(0.0, 0.0, -0.00265, -0.00006, 1.0);
    Planet earth(0.0, 1.0, -6.283185307, 0.0, 4*pow(10,-6));
    Planet jupiter(0.0, -5.2, 2.76148, 0.0, 0.00126667);
    Planet mars(1.52, 0.0, 0.0, 5.08028, 4.4*pow(10,-7));
    Planet venus(-0.72, 0.0, 0.0, -7.378, 3.26667*pow(10,-6));
    Planet saturn(0.0,  9.54, -2.04476, 0.0, 0.000366667); //9.54
    Planet mercury(0.0, -0.39, 10.09732, 0.0, 1.6*pow(10,-7));
    Planet uranus(19.19, 0.0, 0.0, 1.43344, 5.86667*pow(10,-5));
    Planet neptun(0.0, 30.06, -1.2, 0.0, 6.86667*pow(10,-5));//-1.13832
    Planet pluto(0.0, 39.53, 1.1, 0.0, 8.73333*pow(10,-9)); //0.99076

     // Call Solver
    Solver Solarsystem = Solver(N, dt); // give N and dt;

    // Adding planets to Solarsystem
    Solarsystem.initialize_planet(sun);
    Solarsystem.initialize_planet(earth);
    Solarsystem.initialize_planet(jupiter);
    Solarsystem.initialize_planet(mars);
    Solarsystem.initialize_planet(venus);
    Solarsystem.initialize_planet(saturn);
    Solarsystem.initialize_planet(mercury);
    Solarsystem.initialize_planet(uranus);
    Solarsystem.initialize_planet(neptun);
    Solarsystem.initialize_planet(pluto);

    // Call funtions in class Solver
    Solarsystem.q_func();
    Solarsystem.RK4();

    // Does the RK4 method N times on initialvalues and write positions to file
    for(k=0; k < N; k++){
        Solarsystem.RK4();
        outfile1 << Solarsystem.q(0) << " " << Solarsystem.q(1) << endl;
        outfile2 << Solarsystem.q(4) << " " << Solarsystem.q(5) << endl;
        outfile3 << Solarsystem.q(8) << " " << Solarsystem.q(9) << endl;
        outfile4 << Solarsystem.q(12) << " " << Solarsystem.q(13) << endl;
        outfile5 << Solarsystem.q(16) << " " << Solarsystem.q(17) << endl;
        outfile6 << Solarsystem.q(20) << " " << Solarsystem.q(21) << endl;
        outfile7 << Solarsystem.q(24) << " " << Solarsystem.q(25) << endl;
        outfile8 << Solarsystem.q(28) << " " << Solarsystem.q(29) << endl;
        outfile9 << Solarsystem.q(32) << " " << Solarsystem.q(33) << endl;
        outfile10 << Solarsystem.q(36) << " " << Solarsystem.q(37) << endl;
    }

    //cout << Solarsystem.planetlist[0].M << endl;
    //cout << pluto.M << endl;

    outfile1.close();
    outfile2.close();
    outfile3.close();
    outfile4.close();
    outfile4.close();
    outfile5.close();
    outfile6.close();
    outfile7.close();
    outfile8.close();
    outfile9.close();
    outfile10.close();

     return 0;
}


