#include <iostream>
#include <math.h>
#include <armadillo>
#include <fstream>

using namespace std;
using namespace arma;

int main()
{
    int n, m, i, j;
    double dx, dt, alpha;

    fstream outfile1;
    outfile1.open("explicit.dat", ios::out);


    n = 8;
    m = 100;

    dx = 1.0/(n+1);
    dt = 0.5*pow(dx,2)-0.00001;
    alpha = dt/pow(dx,2);

    vec v_new = zeros(n+2);
    vec v_old = zeros(n+2);
    vec u_s(n+2);

    for(i=1; i<n+1; i++){
        v_old(i) = -1 + i*dx;
    }

    for(i=0; i < n+1; i++){
        u_s(i) = 1 - i*dx;
    }

    v_old(0) = 0;
    v_old(n+1) = 0;

    for(j=1; j <= m; j++){
        for(i=1; i<n+1; i++){
            //v_new(i) = alpha * v_old(i-1) + (1 - 2*alpha) * v_old(i) + alpha * v_old(i+1);
            v_new(i) = v_old(i) + alpha*(v_old(i+1)-2*v_old(i)+v_old(i-1));
        }

        v_old = v_new;
        for(i=0; i <=n+1; i++){
            outfile1 << v_new(i)+u_s(i) << " ";
        }
        outfile1 << endl;
    }
    outfile1.close();
    return 0;
}


/*
#include <iostream>
#include <math.h>
#include <armadillo>
#include <fstream>

using namespace std;
using namespace arma;

int main()
{
    int n, m, i, j;
    double dx, dt;



    n = 10;
    m = 100;

    dx = 1.0/(n+1);
    dt = 0.5*pow(dx,2)-0.00001;


    mat u1 = zeros(n,m);

    fstream outfile1;
    outfile1.open("explicit.dat", ios::out);

    // Forward Euler
    for(j=1; j < m-1; j++){
        for(i=1; i<n-1; i++){

            u1(i,0) = 0.0;
            u1(0,j+1) = 1.0; // +1 ??
            u1(n-1,j) = 0.0;
            u1(0,0) = 1.0;

            u1(i, j+1) = u1(i,j) + (dt/pow(dx,2))*(u1(i+1,j)-2*u1(i,j)+u1(i-1,j));
        }

    }

    for(j=1; j < m; j++){
        for(i=0; i<n; i++){
            cout << u1(i,j) << " ";
        }
        cout << endl;
    }

    for(j=1; j < m; j++){
        for(i=0; i<n; i++){
            outfile1 << u1(i,j) << " ";
        }
        outfile1 << endl;
    }





    outfile1.close();
    return 0;
}
*/
