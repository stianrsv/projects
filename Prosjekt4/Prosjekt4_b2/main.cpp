#include <iostream>
#include <math.h>
#include <armadillo>
#include <fstream>

using namespace std;
using namespace arma;

int main()
{

    int n, m, i, j;
    double dx, dt, alpha;


    fstream outfile1;
    outfile1.open("implicit.dat", ios::out);


    n = 8;
    m = 100;

    dx = 1.0/(n+1);
    dt = 0.5*pow(dx,2)-0.00001;


    // Backward Euler
    //u2(i, j-1) = u2(i,j) - (dt/pow(dx,2))*(u2(i+1,j)-2*u2(i,j)+u2(i-1,j));


    vec v_new = zeros(n+2);
    vec v_old(n+2);

    vec a(n+2);
    vec b(n+2);
    vec c(n+2);

    alpha = dt/pow(dx,2);
    a.fill(-alpha);
    b.fill(1.0+2.0*alpha);
    c.fill(-alpha);
    v_old.fill(0);

    vec u_s(n+2);


    for(i=1; i<n+1; i++){
        v_old(i) = -1 + i*dx;
    }

    for(i=0; i<n+1; i++){
        u_s(i) = 1 - i*dx;
    }

    //cout << v_old << endl;
    //cout << u_s << endl;

    for(j=0; j < m; j++){
        b.fill(1.0+2.0*alpha);

        // Forward substitution with our own algorithm
        for(i=2; i <= n; i++){
            b(i) = b(i)-(a(i)*c(i-1))/b(i-1);
            v_old(i) = v_old(i)-(a(i)*v_old(i-1))/b(i-1);
        }

        // Set initial conditions
        v_new(0) = 0.0;
        v_new(n+1) = 0.0;

        // Backward substitution with our own algorithm
        v_new(n) = v_old(n)/b(n);

        for(i=n; i >= 2; i--){
            v_new(i-1) = (v_old(i-1)+(-c(i-1))*v_new(i))/b(i-1);
        }

        v_old = v_new;

        for(i=0; i <=n+1; i++){
            cout << v_new(i)+u_s(i) << " ";
        }

        for(i=0; i <=n+1; i++){
            outfile1 << v_new(i)+u_s(i) << " ";
        }
        outfile1 << "0" << endl;
    }
    return 0;
}








/*

    // Forward Euler
    u_t = (u(x(i),t(j)+dt)-u(x(i),t(j)))/dt;
    u_xx = (u(x(i)+dx,t(j))-2*u(x(i),t(j))+u(x(i)-dx,t(j)))/pow(dx,2);


    // Backward Euler
    u_t = (u(x(i),t(j))-u(x(i),t(j)-dt))/dt;
    u_xx = (u(x(i)+dx,t(j))-2*u(x(i),t(j))+u(x(i)-dx,t(j)))/pow(dx,2);

    // Crank-Nicolson
    u_t = (u(x(i),t(j)+dt)-u(x(i),t(j)))/dt;
    u_xx = 0.5*((u(x(i)+dx,t(j))-2*u(x(i),t(j))+u(x(i)-dx,t(j)))/pow(dx,2) + (u(x(i)+dx,t(j)+dt)-2*u(x(i),t(j)+dt)+u(x(i)-dx,t(j)+dt))/pow(dx,2));;
    */
