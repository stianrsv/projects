#include <iostream>
#include <math.h>
#include <armadillo>
#include <fstream>

using namespace std;
using namespace arma;

int main()
{

    int n, m, i, j, k;
    double dx, dt, alpha;
    fstream outfile1;
    outfile1.open("crank-nicolson.dat", ios::out);


    n = 8;
    m = 3;

    dx = 1.0/(n+1);
    dt = 0.5*pow(dx,2)-0.00001;
    alpha = dt/pow(dx,2);

    // A*u = f
    // A*v_new = v_old
    vec v_new = zeros(n+2);
    vec v_old = zeros(n+2);
    vec a(n+2);
    vec b(n+2);
    vec c(n+2);
    vec u_s(n+2);

    a.fill(-alpha);
    c.fill(-alpha);

    for(i=1; i<n+1; i++){
        v_new(i) = -1 + i*dx;
    }

    for(i=0; i < n+1; i++){
        u_s(i) = 1 - i*dx;
    }

    v_old(0) = 0;
    v_old(n+1) = 0;
    for(j=1; j <= m; j++){
        for(i=1; i<n+1; i++){
            v_old(i) =  alpha*v_new(i-1)   +   (2.0 - 2*alpha)*v_new(i)   +   alpha*v_new(i+1);
        }

        v_old(0) = 0;
        v_old(n+1) = 0;

        for(k=1; k <= m; k++){
            b.fill(2*(1+alpha));

            // Forward substitution with our own algorithm
            for(i=1; i < n+1; i++){
                b(i) = b(i)-(a(i)*c(i-1))/b(i-1);
                v_old(i) = v_old(i)-(a(i)*v_old(i-1))/b(i-1);
            }

            // Set initial conditions
            v_new(0) = 0.0;
            v_new(n+1) = 0.0;

            // Backward substitution with our own algorithm
            v_new(n) = v_old(n)/b(n);

            for(i=n; i >= 2; i--){
                v_new(i-1) = (v_old(i-1)+(-c(i-1))*v_new(i))/b(i-1);
            }


            v_old = v_new;

            for(i=0; i <=n+1; i++){
                cout << v_new(i)+u_s(i) << " ";
            }
            cout << endl;


            for(i=0; i <=n+1; i++){
                outfile1 << v_new(i)+u_s(i) << " ";
            }
            cout << endl;


        }

        outfile1.close();
        return 0;
    }

}
