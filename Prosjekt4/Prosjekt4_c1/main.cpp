#include <iostream>
#include <math.h>
#include <armadillo>
#include <fstream>

using namespace std;
using namespace arma;


const double pi = 4*atan(1.0);

int main()
{
    int i, j, k, n, m, l;
    double sum, dx, dt;

    fstream outfile1;
    outfile1.open("analytic.dat", ios::out);

    n = 100; // sum
    m = 100; // time steps
    l = 10; // x values

    dx = 1.0/10.0;
    dt = 0.5*pow(dx,2)-0.00001;

    vec u_new = zeros(l);

    for(j=0; j < m; j++){
        for(k=0; k < l; k++){
            sum = 0.0;
            for(i=1; i < n; i++){
                sum += (1.0/i)*sin(i*pi*(k*dx))*exp(-i*i*pi*pi*(j*dt));
            }
            u_new(k) = 1.0 - (k*dx) - (2.0/(pi))*sum;
            cout << u_new(k) << " ";
            outfile1 << u_new(k) << " ";
        }
        cout << endl;
        outfile1 << endl;
    }
    outfile1.close();
    return 0;
}




