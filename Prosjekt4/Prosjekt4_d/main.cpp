#include <iostream>
#include <math.h>
#include <armadillo>
#include <fstream>

using namespace std;
using namespace arma;

int main()
{
    int n, m, i, j, k, h;
    double dx, dt, alpha, T;

    n = 8;
    m = 3;

    dx = 1.0/(n+1);
    //dx = 1.0/10.0;
    //dx = 1.0/100.0;

    dt = 0.5*pow(dx,2)-0.00001;
    alpha = dt/pow(dx,2);

    //T0 = dt*(n+1);  // 0.05, og 0.5
    //T1





    // Setting up vectors and initial conditions
    vec v_new = zeros(n+2);
    vec v_old = zeros(n+2);
    vec u_s(n+2);

    vec v_new2 = zeros(n+2);
    vec v_old2(n+2);
    vec a(n+2);
    vec b(n+2);
    vec c(n+2);
    vec u_s2(n+2);
    a.fill(-alpha);
    b.fill(1.0+2.0*alpha);
    c.fill(-alpha);
    v_old2.fill(0);

    vec v_new3 = zeros(n+2);
    vec v_old3 = zeros(n+2);
    vec a3(n+2);
    vec b3(n+2);
    vec c3(n+2);
    vec u_s3(n+2);
    a3.fill(-alpha);
    c3.fill(-alpha);

    for(i=1; i<n+1; i++){
        v_old(i) = -1 + i*dx;
    }
    for(i=0; i < n+1; i++){
        u_s(i) = 1 - i*dx;
    }
    v_old(0) = 0;
    v_old(n+1) = 0;


    for(i=1; i<n+1; i++){
        v_old2(i) = -1 + i*dx;
    }
    for(i=0; i<n+1; i++){
        u_s2(i) = 1 - i*dx;
    }


    for(i=1; i<n+1; i++){
        v_new3(i) = -1 + i*dx;
    }
    for(i=0; i < n+1; i++){
        u_s3(i) = 1 - i*dx;
    }
    v_old3(0) = 0;
    v_old3(n+1) = 0;


    for(j=1; j <= m; j++){
        //--------------------------------------------------------------------------------------
        // Explicit

        for(i=1; i<n+1; i++){
            v_new(i) = v_old(i) + alpha*(v_old(i+1)-2*v_old(i)+v_old(i-1));
        }
        v_old = v_new;


        // ---------------------------------------------------------------------------------------------
        // Implicit

        b.fill(1.0+2.0*alpha);

        // Forward substitution with our own algorithm
        for(i=2; i <= n; i++){
            b(i) = b(i)-(a(i)*c(i-1))/b(i-1);
            v_old2(i) = v_old2(i)-(a(i)*v_old2(i-1))/b(i-1);
        }

        // Set initial conditions
        v_new2(0) = 0.0;
        v_new2(n+1) = 0.0;

        // Backward substitution with our own algorithm
        v_new2(n) = v_old2(n)/b(n);

        for(i=n; i >= 2; i--){
            v_new2(i-1) = (v_old2(i-1)+(-c(i-1))*v_new2(i))/b(i-1);
        }
        v_old2 = v_new2;



        // -------------------------------------------
        // Solutions Explicit and Implicit


        for(i=0; i <=n+1; i++){
            //cout << v_new(i)+u_s(i) << " ";
        }
        //cout << endl;

        for(i=0; i <=n+1; i++){
            //cout << v_new2(i)+u_s2(i) << " ";
        }
        //cout << endl;

    }

    //------------------------------------------------------------------------------
    // Crank-Nicolson


    for(k=1; k <= m; k++){
        for(i=1; i<n+1; i++){
            v_old3(i) =  alpha*v_new3(i-1)   +   (2.0 - 2*alpha)*v_new3(i)   +   alpha*v_new3(i+1);
        }

        v_old3(0) = 0;
        v_old3(n+1) = 0;

        for(k=1; k <= m; k++){
            b3.fill(2*(1+alpha));

            // Forward substitution with our own algorithm
            for(i=1; i < n+1; i++){
                b3(i) = b3(i)-(a3(i)*c3(i-1))/b3(i-1);
                v_old3(i) = v_old3(i)-(a3(i)*v_old3(i-1))/b3(i-1);
            }

            // Set initial conditions
            v_new3(0) = 0.0;
            v_new3(n+1) = 0.0;

            // Backward substitution with our own algorithm
            v_new3(n) = v_old3(n)/b3(n);

            for(i=n; i >= 2; i--){
                v_new3(i-1) = (v_old3(i-1)+(-c3(i-1))*v_new3(i))/b3(i-1);
            }

            v_old3 = v_new3;
            for(i=0; i <=n+1; i++){
                cout << v_new3(i)+u_s3(i) << " ";
            }
            cout << endl;
        }
    }
    return 0;
}
