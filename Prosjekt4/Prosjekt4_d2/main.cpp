// This program computes the development it time for the diffusion equation for neurotransmitters with tree different
// numerical method, explcit, implcit and Crank-Nicholson, and compute the error compared to the analytical solution
// dx = 1/10


#include <iostream>
#include <math.h>
#include <armadillo>
#include <fstream>

using namespace std;
using namespace arma;

const double pi = 4*atan(1.0);

int main()
{
    int n, n2, m, i, j, k, l, p, g;
    double dx, dt, alpha, T0, T1, sum;


    fstream outfile1, outfile2, outfile3, outfile4, outfile5, outfile6, outfile7, outfile8;
    outfile1.open("explicit_error_10.dat", ios::out);
    outfile2.open("implicit_error_10.dat", ios::out);
    outfile3.open("crank-nicolson_error_10.dat", ios::out);
    outfile5.open("explicit.dat", ios::out);
    outfile6.open("implicit.dat", ios::out);
    outfile7.open("crank-nicolson.dat", ios::out);
    outfile8.open("analytic.dat", ios::out);




    n = 9;// x-values
    m = 100; // t steps


    dx = 1.0/10.0;
    //dx = 1.0/100.0;

    dt = 0.5*pow(dx,2);
    alpha = dt/pow(dx,2);

    //cout << 0.05/dt << endl;
    //cout << 0.5/dt << endl;

    T0 = dt*10;  // 0.05
    T1 = dt*100;  // 0,5


    // Setting up vectors and initial conditions
    vec v_new = zeros(n+2);
    vec v_old = zeros(n+2);
    vec u_s(n+2);

    vec v_new2 = zeros(n+2);
    vec v_old2(n+2);
    vec a(n+2);
    vec b(n+2);
    vec c(n+2);
    vec u_s2(n+2);
    a.fill(-alpha);
    b.fill(1.0+2.0*alpha);
    c.fill(-alpha);
    v_old2.fill(0);

    vec v_new3 = zeros(n+2);
    vec v_old3 = zeros(n+2);
    vec a3(n+2);
    vec b3(n+2);
    vec c3(n+2);
    vec u_s3(n+2);
    a3.fill(-alpha);
    c3.fill(-alpha);


    for(i=1; i<n+1; i++){
        v_old(i) = -1 + i*dx;
    }
    for(i=0; i < n+1; i++){
        u_s(i) = 1 - i*dx;
    }
    v_old(0) = 0;
    v_old(n+1) = 0;


    for(i=1; i<n+1; i++){
        v_old2(i) = -1 + i*dx;
    }
    for(i=0; i<n+1; i++){
        u_s2(i) = 1 - i*dx;
    }


    for(i=1; i<n+1; i++){
        v_new3(i) = -1 + i*dx;
    }
    for(i=0; i < n+1; i++){
        u_s3(i) = 1 - i*dx;
    }


    //---------------------------------------------------------
    // Analytic solution

    l = n+2; // x-values
    p = 100; // sum loop
    vec u_new = zeros(l);

    for(j=0; j < m; j++){
        for(k=0; k < l; k++){
            sum = 0.0;
            for(i=1; i < p; i++){
                sum += (1.0/i)*sin(i*pi*(k*dx))*exp(-i*i*pi*pi*(j*dt));
            }
            u_new(k) = 1.0 - k*dx - (2.0/(pi))*sum;
            //cout << u_new(k) << " ";
        }
        //cout << endl;
    }

    //----------------------------------------------------------

    for(j=1; j <= m; j++){

        //------------------------------------------------------
        // Explicit

        for(i=1; i<n+1; i++){
            v_new(i) = v_old(i) + alpha*(v_old(i+1)-2*v_old(i)+v_old(i-1));
        }
        v_old = v_new;


        // -----------------------------------------------------
        // Implicit

        b.fill(1.0+2.0*alpha);

        // Forward substitution with our own algorithm
        for(i=1; i <= n; i++){
            b(i) = b(i)-(a(i)*c(i-1))/b(i-1);
            v_old2(i) = v_old2(i)-(a(i)*v_old2(i-1))/b(i-1);
        }

        // Set initial conditions
        v_new2(0) = 0.0;
        v_new2(n+1) = 0.0;

        // Backward substitution with our own algorithm
        v_new2(n) = v_old2(n)/b(n);

        for(i=n; i >= 2; i--){
            v_new2(i-1) = (v_old2(i-1)+(-c(i-1))*v_new2(i))/b(i-1);
        }
        v_old2 = v_new2;



        // -------------------------------------------
        // Solutions Explicit and Implicit


        for(i=0; i <=n+1; i++){
            //cout << v_new(i)+u_s(i) << " ";
        }
        //cout << endl;

        for(i=0; i <=n+1; i++){
            //cout << v_new2(i)+u_s2(i) << " ";
        }
        //cout << endl;

    }

    //------------------------------------------------------------------------------
    // Crank-Nicholson

    for(k=1; k <= m; k++){
        for(i=1; i<n+1; i++){
            v_old3(i) =  alpha*v_new3(i-1)   +   (2.0 - 2*alpha)*v_new3(i)   +   alpha*v_new3(i+1);
        }

        v_old3(0) = 0;
        v_old3(n+1) = 0;

        b3.fill(2+2*alpha);


        // Forward substitution with our own algorithm
        for(i=1; i <= n; i++){
            b3(i) = b3(i)-(a3(i)*c3(i-1))/b3(i-1);
            v_old3(i) = v_old3(i)-(a3(i)*v_old3(i-1))/b3(i-1);
        }

        // Set initial conditions
        v_new3(0) = 0.0;
        v_new3(n+1) = 0.0;

        // Backward substitution with our own algorithm
        v_new3(n) = v_old3(n)/b3(n);

        for(i=n; i >= 2; i--){
            v_new3(i-1) = (v_old3(i-1)+(-c3(i-1))*v_new3(i))/b3(i-1);
        }

        v_old3 = v_new3;

        for(i=0; i <=n+1; i++){
            //cout << v_new3(i)+u_s3(i) << " ";
        }
        //cout << endl;
    }

    //cout << u_new << " ";
    //cout << v_new+u_s << " ";
    //cout << v_new2+u_s2 << " ";
    //cout << v_new3+u_s3 << " ";


    vec dx_vec = zeros(n+2);
    for(g=0; g < n+2; g++){
        dx_vec(g) = (dx*g);
    }

/*
    cout << abs(u_new-(v_new+u_s)) << endl;
    cout << abs(u_new-(v_new2+u_s2)) << endl;
    cout << abs(u_new-(v_new3+u_s3)) << endl;
    cout << dx_vec;

*/
    outfile1 << (abs(u_new-(v_new+u_s))) << endl;
    outfile2 << (abs(u_new-(v_new2+u_s2))) << endl;
    outfile3 << (abs(u_new-(v_new3+u_s3))) << endl;
    outfile4 << dx_vec;
    outfile5 << v_new+u_s << endl;
    outfile6 << v_new2+u_s2 << endl;
    outfile7 << v_new3+u_s3 << endl;
    outfile8 << u_new << endl;



    outfile1.close();
    outfile2.close();
    outfile3.close();
    outfile4.close();
    outfile5.close();
    outfile6.close();
    outfile7.close();
    outfile8.close();
    return 0;
}

