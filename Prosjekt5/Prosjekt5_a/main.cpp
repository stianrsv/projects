// This program simulate the Sun-Earth system, computes energies of the system,
// computes error in energies, compute time usage
// We can easily change resolution and simulation time, and run over many different values of "dt"


#include <iostream>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;
using namespace arma;

const double pi = 4*atan(1.0);
const double G = 4*pow(pi,2);


// Class to set values corresponding different planets
class Planet {
public:
    double x, y, z, vx, vy, vz, M;
    Planet(double, double, double, double, double, double, double);
    Planet() {}
};

Planet::Planet(double x, double y, double z, double vx, double vy, double vz, double M){
    this->x = x;
    this->y = y;
    this->z = z;
    this->vx = vx;
    this->vy = vy;
    this->vz = vz;
    this->M = M;
}

// Class to use RK4 to solve the differential equations
class Solver {
public:
    int n, N;
    double dt;
    vec lf1;
    vec lf2;
    vec f(vec l);
    Planet planetlist[1001]; // random number higher than number of planets
    Solver (int N_value, double dt_value);
    void initial_func();
    void initialize_planet(Planet name);
    void RK4();
    void LF();
    double Energy();
};

// Function to add planets to the solarsystem
void Solver::initialize_planet(Planet name){
    planetlist[n] = name;
    n++;
}

// Constructor
Solver::Solver(int N_value, double dt_value){
    n = 0;
    N = N_value;
    dt = dt_value;
}

// Function to initialize initialvalues
void Solver::initial_func(){
    lf1 = zeros(3*n);
    lf2 = zeros(3*n);

    for(int i=0; i<n; i++){
        lf1[3*i] = planetlist[i].x;
        lf1[3*i+1] = planetlist[i].y;
        lf1[3*i+2] = planetlist[i].z;

        lf2[3*i] = planetlist[i].vx;
        lf2[3*i+1] = planetlist[i].vy;
        lf2[3*i+2] = planetlist[i].vz;
    }
}


// Function to call in RK4
vec Solver::f(vec lf1){
    double r, f_force, x, y, z;
    int i, j;
    vec f_values = zeros(3*n);
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            x = lf1(3*i) - lf1(3*j);
            y = lf1(3*i+1) - lf1(3*j+1);
            z = lf1(3*i+2) - lf1(3*j+2);
            r = sqrt(x*x + y*y + z*z);
            if (i != j){
                f_force = -G*planetlist[j].M/(pow(r,3));
                f_values(3*i) += f_force*x;
                f_values(3*i+1) += f_force*y;
                f_values(3*i+2) += f_force*z;
            }
        }
    }
    return f_values;
}

// RK4 method
void Solver:: RK4(){
    vec k1 = zeros(3*n);
    vec k2 = zeros(3*n);
    vec k3 = zeros(3*n);
    vec k4 = zeros(3*n);
    vec k_1 = zeros(3*n);
    vec k_2 = zeros(3*n);
    vec k_3 = zeros(3*n);
    vec k_4 = zeros(3*n);

    k1 = f(lf1)*dt;
    k_1 = lf2*dt;
    k2 = f(lf1 + 0.5*k_1)*dt;
    k_2 = (lf2 + 0.5*k1)*dt;
    k3 = f(lf1 + 0.5*k_2)*dt;
    k_3 = (lf2 + 0.5*k2)*dt;
    k4 = f(lf1 + k_3)*dt;
    k_4 = (lf2 + k3)*dt;

    lf1 += (1.0/6.0)*(k_1 + 2.0*k_2 + 2.0*k_3 + k_4);
    lf2 += (1.0/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
}


// Leap-Frog method
void Solver:: LF(){
    vec dx_h_half = zeros(3*n);
    vec x_h = zeros(3*n);
    vec dx_h = zeros(3*n);

    dx_h_half = lf2 + 0.5*dt*f(lf1);
    x_h = lf1 + dt*dx_h_half;
    dx_h = dx_h_half + 0.5*dt*f(x_h);

    lf1 = x_h;
    lf2 = dx_h;
}


// Function to compute kinetic and potential energy og the system
double Solver:: Energy(){
    double v, r, e_kin, e_pot, e_kin_tot, e_pot_tot, energy_tot;
    e_kin_tot = 0.0;
    e_pot_tot = 0.0;
    energy_tot = 0.0;
    e_kin = 0.0;

    for(int k=0; k < n; k++){
        v = sqrt(pow(lf2(3*k),2)+pow(lf2(1+3*k),2)+pow(lf2(2+3*k),2));
        e_kin = 0.5*planetlist[k].M*pow(v,2);

        e_pot = 0.0;
        for(int p=0; p < n; p++){
            if(k != p){
                r =  sqrt(pow(lf1(3*k) - lf1(3*p),2)+pow(lf1(1+3*k) - lf1(1+3*p),2)+pow(lf1(2+3*k) - lf1(2+3*p),2));
                e_pot += (-G*planetlist[k].M*planetlist[p].M)/r;
            }
        }

        e_pot_tot += e_pot/2.0;
        e_kin_tot += e_kin;
        energy_tot = e_pot_tot + e_kin_tot;
        return energy_tot;
    }
}


int main(){

    int N, k;
    double dt;

    N = 10000000; // number of timesteps
    dt = 0.001; // length of steps in RK4/LF

    fstream outfile1, outfile2, outfile3, outfile4;
    outfile1.open("energy_RK4.dat", ios::out);
    outfile2.open("energy_LF.dat", ios::out);
    outfile3.open("error_RK4.dat", ios::out);
    outfile4.open("error_LF.dat", ios::out);



    // Uncomment loop to compute energy dependence on different dt-values
    //for(p=1; p < 3000; p++){
    //if(dt < 1){
    //dt = 0.000000001*pow(10,p/30.0);

    // Initial_values for all planets(x, y, z, vx, vy, vz, M)
    Planet sun(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    Planet earth(0.0, 1.0, 0.1, -6.283185307, 0.0, 0.0, 4*pow(10,-6));

    // Call Solver
    Solver Solarsystem = Solver(N, dt); // give N and dt;

    // Adding planets to Solarsystem
    Solarsystem.initialize_planet(sun);
    Solarsystem.initialize_planet(earth);

    // Start clock to compute spend time
    double time1;
    clock_t start1, finish1;
    start1 = clock();

    // Does the RK4 method N times on initialvalues and write positions to file
/*
    Solarsystem.initial_func();
    for(k=0; k < N; k++){
        Solarsystem.RK4();
        //cout << Solarsystem.lf1(3) << " " << Solarsystem.lf1(4) << " " << Solarsystem.lf1(5) << endl;

        //outfile1 << (k*dt) << " ";
        //outfile1 << Solarsystem.Energy() << endl;

    }
    //outfile3 << dt << " " << Solarsystem.Energy() << endl;
*/


    // Does the LF method N times on initialvalues and write positions to file
    Solarsystem.initial_func();
    for(k=0; k < N; k++){
        Solarsystem.LF();
        cout << Solarsystem.lf1(3) << " " << Solarsystem.lf1(4) << " " << Solarsystem.lf1(5) << endl;
        outfile2 << (k*dt) << " ";
        outfile2 << Solarsystem.Energy() << endl;

    }
    outfile4 << dt << " " << Solarsystem.Energy() << endl;

    // Stop the clock and estimate the spend time
    finish1 = clock();
    time1 = ((finish1 - start1)/((double) CLOCKS_PER_SEC));
    cout << time1/N << endl;

    outfile1.close();
    outfile2.close();
    outfile3.close();
    outfile4.close();

    return 0;
}
