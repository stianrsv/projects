TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp
LIBS += -larmadillo -lblas -llapack


QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS +=  -fopenmp

QMAKE_CXX = ccache g++
