// This program simulate an N-body cluster, computes different energy parameters,
// computes position of masscenter, computes radius between masscenter and all bounded particles
// We can easily change number of stars, resolution and simulation time


#include <iostream>
#include <math.h>
#include <cmath>
#include <armadillo>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include "gaussiandeviate.cpp"

using namespace std;
using namespace arma;

const double pi = 4*atan(1.0);

// Class to set values corresponding different stars
class Star {
public:
    double x, y, z, vx, vy, vz, M;
    Star(double, double, double, double, double, double, double);
    Star() {}
};

Star::Star(double x, double y, double z, double vx, double vy, double vz, double M){
    this->x = x;
    this->y = y;
    this->z = z;
    this->vx = vx;
    this->vy = vy;
    this->vz = vz;
    this->M = M;
}

// Class to use RK4 or Leap-Frog to solve the differential equations
class Solver {
public:
    int n, N;
    double dt, G;
    Solver (int N_value, double dt_value, double G);
    Star starlist[10001]; // random number higher than number of stars
    vec lf1;
    vec lf2;
    vec f(vec l);
    void Stars(int);
    void initial_func();
    void initialize_planet(Star name);
    void RK4();
    void LF();
    void Masscenter(double *masscenter_position_x,  double *masscenter_position_y,  double *masscenter_position_z);
    double Energy(double *escaped, double *potential, double *kinetic,  double *ep_escaped, double *ek_escaped);

};


// Constructor
Solver::Solver(int N, double dt, double G){
    this->N = N;
    this->dt = dt;
    this->G = G;
    n = 0;
}


// Function to add stars to the cluster
void Solver::initialize_planet(Star name){
    starlist[n] = name;
    n++;
}


// Function who setting up the initial random distribution in a sphere
void Solver::Stars(int N_stars){
    Star x;

    long seed = clock(); //giving the same random numbers each simulation
    //time_t seed = time(0); //giving new random numbers each simulation
    for(int i=0; i < N_stars; i++){
        vec rand_num = randu(3);

        double R0 = 20.0; // maximum initial radius stars

        double r_rand = pow(rand_num(0),(1.0)/3.0)*R0;
        double tetta = atan(2.0*rand_num(1)-1.0)+(pi/2.0);
        double phi = rand_num(2)*2*pi;

        double x_rand = r_rand*sin(tetta)*cos(phi);
        double y_rand = r_rand*sin(tetta)*sin(phi);
        double z_rand = r_rand*cos(tetta);

        double vx = 0.0;
        double vy = 0.0;
        double vz = 0.0;

        double M_gaussian = (sqrt(2.0)*gaussian_deviate(&seed)) + 10; // gaussian distribution around 10 sun masses, deviation 1 sun mass

        x = Star(x_rand, y_rand, z_rand, vx, vy, vz, M_gaussian);
        this->initialize_planet(x);
    }
}


// Function to initialize initialvalues
void Solver::initial_func(){
    lf1 = zeros(3*n);
    lf2 = zeros(3*n);

    for(int i=0; i<n; i++){
        lf1[3*i] = starlist[i].x;
        lf1[3*i+1] = starlist[i].y;
        lf1[3*i+2] = starlist[i].z;

        lf2[3*i] = starlist[i].vx;
        lf2[3*i+1] = starlist[i].vy;
        lf2[3*i+2] = starlist[i].vz;
    }
}


// Function to compute forces between stars
vec Solver::f(vec lf1){
    int i, j;
    double r, f_force, x, y, z, epsilon;
    epsilon = 0.1; // 0.1 smoothing value
    vec f_values = zeros(3*n);
    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            x = lf1(3*i) - lf1(3*j);
            y = lf1(3*i+1) - lf1(3*j+1);
            z = lf1(3*i+2) - lf1(3*j+2);
            r = sqrt(x*x + y*y + z*z);
            if (i != j){
                f_force = -G*starlist[j].M/((pow(r,3))+(pow(epsilon,2)*r));
                f_values(3*i) += f_force*x;
                f_values(3*i+1) += f_force*y;
                f_values(3*i+2) += f_force*z;
            }
        }
    }
    return f_values;
}


// RK4 method
void Solver:: RK4(){
    vec k1 = zeros(3*n);
    vec k2 = zeros(3*n);
    vec k3 = zeros(3*n);
    vec k4 = zeros(3*n);
    vec k_1 = zeros(3*n);
    vec k_2 = zeros(3*n);
    vec k_3 = zeros(3*n);
    vec k_4 = zeros(3*n);

    k1 = f(lf1)*dt;
    k_1 = lf2*dt;
    k2 = f(lf1 + 0.5*k_1)*dt;
    k_2 = (lf2 + 0.5*k1)*dt;
    k3 = f(lf1 + 0.5*k_2)*dt;
    k_3 = (lf2 + 0.5*k2)*dt;
    k4 = f(lf1 + k_3)*dt;
    k_4 = (lf2 + k3)*dt;

    lf1 += (1.0/6.0)*(k_1 + 2.0*k_2 + 2.0*k_3 + k_4);
    lf2 += (1.0/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);
}


// Leap-Frog method
void Solver:: LF(){
    vec dx_h_half = zeros(3*n);
    vec x_h = zeros(3*n);
    vec dx_h = zeros(3*n);

    dx_h_half = lf2 + 0.5*dt*f(lf1);
    x_h = lf1 + dt*dx_h_half;
    dx_h = dx_h_half + 0.5*dt*f(x_h);

    lf1 = x_h;
    lf2 = dx_h;
}


// Function who calculate different energy-parameters
double Solver:: Energy(double *escaped, double *potential, double *kinetic, double *ep_escaped, double *ek_escaped){
    double v, r, e_kin, e_pot, e_kin_tot, e_pot_tot, energy_tot;
    e_kin_tot = 0.0;
    e_pot_tot = 0.0;
    energy_tot = 0.0;
    *potential = 0.0;
    *kinetic = 0.0;
    *escaped = 0;
    *ep_escaped = 0.0;
    *ek_escaped = 0.0;

    e_kin = 0.0;
    for(int k=0; k < n; k++){
        v = sqrt(pow(lf2(3*k),2)+pow(lf2(1+3*k),2)+pow(lf2(2+3*k),2));
        e_kin = 0.5*starlist[k].M*pow(v,2);

        e_pot = 0.0;
        for(int p=0; p < n; p++){
            if(k != p){
                r =  sqrt(pow(lf1(3*k) - lf1(3*p),2)+pow(lf1(1+3*k) - lf1(1+3*p),2)+pow(lf1(2+3*k) - lf1(2+3*p),2));
                e_pot += (-G*starlist[k].M*starlist[p].M)/r;
            }
        }

        e_pot_tot += e_pot/2.0;
        e_kin_tot += e_kin;

        if(e_kin > fabs(e_pot)){
            *escaped += 1.0;
            *ep_escaped += e_pot;
            *ek_escaped += e_kin;
        }
    }
    *potential = (e_pot_tot - *ep_escaped);
    *kinetic = (e_kin_tot - *ek_escaped);
    energy_tot = e_pot_tot + e_kin_tot;
    return energy_tot;
}


// Function who compute position of the massenter of the cluser and the radial distribution
void Solver:: Masscenter(double *masscenter_position_x,  double *masscenter_position_y,  double *masscenter_position_z){
    double v, r, e_kin, e_pot;
    double total_mass_equilibrium, position_x, position_y, position_z;
    double sum_masscenter_x, sum_masscenter_y, sum_masscenter_z, radial_single,sum_radial_single, sum_radial_single_sqare, radial_averange, radial_max;

    total_mass_equilibrium = 0.0;
    sum_masscenter_x = 0.0;
    sum_masscenter_y = 0.0;
    sum_masscenter_z = 0.0;
    sum_radial_single = 0.0;
    sum_radial_single_sqare = 0.0;

    e_kin = 0.0;
    for(int k=0; k < n; k++){
        v = sqrt(pow(lf2(3*k),2)+pow(lf2(1+3*k),2)+pow(lf2(2+3*k),2));
        e_kin = 0.5*starlist[k].M*pow(v,2);

        e_pot = 0.0;
        for(int p=0; p < n; p++){
            if(k != p){
                r =  sqrt(pow(lf1(3*k) - lf1(3*p),2)+pow(lf1(1+3*k) - lf1(1+3*p),2)+pow(lf1(2+3*k) - lf1(2+3*p),2));
                e_pot += (-G*starlist[k].M*starlist[p].M)/r;
            }
        }

        if(e_kin <= fabs(e_pot)){
            total_mass_equilibrium += starlist[k].M;
            position_x = lf1(3*k);
            position_y = lf1(1+3*k);
            position_z = lf1(2+3*k);
            sum_masscenter_x += starlist[k].M*position_x;
            sum_masscenter_y += starlist[k].M*position_y;
            sum_masscenter_z += starlist[k].M*position_z;
        }
    }
    *masscenter_position_x = (1.0/total_mass_equilibrium)*(sum_masscenter_x);
    *masscenter_position_y = (1.0/total_mass_equilibrium)*(sum_masscenter_y);
    *masscenter_position_z = (1.0/total_mass_equilibrium)*(sum_masscenter_z);


    radial_max = 25.0;

    int n_resolution = 25;
    double r_steps = radial_max/n_resolution;

    vec r_resolution = zeros(n_resolution+1);
    vec histogram = zeros(n_resolution);
    vec r_plotting_points = zeros(n_resolution);

    for(int j=0; j < n_resolution+1; j++){
        r_resolution(j) = r_steps*j;
    }

    for(int j=0; j < n_resolution; j++){
        r_plotting_points(j) = r_steps*j + 0.5*r_steps;
    }

    int N_total;
    e_kin = 0.0;
    int number_of_bounded_stars = 0;
    for(int k=0; k < n; k++){
        N_total    += 1;
        v = sqrt(pow(lf2(3*k),2)+pow(lf2(1+3*k),2)+pow(lf2(2+3*k),2));
        e_kin = 0.5*starlist[k].M*pow(v,2);

        e_pot = 0.0;
        for(int p=0; p < n; p++){
            if(k != p){
                r =  sqrt(pow(lf1(3*k) - lf1(3*p),2)+pow(lf1(1+3*k) - lf1(1+3*p),2)+pow(lf1(2+3*k) - lf1(2+3*p),2));
                e_pot += (-G*starlist[k].M*starlist[p].M)/r;
            }
        }
        if(e_kin <= fabs(e_pot)){
            number_of_bounded_stars += 1;
            radial_single =  sqrt(pow(lf1(3*k)-*masscenter_position_x,2) + pow(lf1(1+3*k)-*masscenter_position_y,2) + pow(lf1(2+3*k)-*masscenter_position_z,2));
            sum_radial_single += radial_single;
            sum_radial_single_sqare += pow(radial_single,2);

            for(int j=0; j < n_resolution; j++){
                if((r_resolution(j) <= radial_single) && (radial_single < r_resolution(j+1))){
                    histogram(j) += 1;
                }
            }
        }
    }

    fstream histogramfile, histogramfile2, radial_distributionfile;
    //histogramfile.open("histogram_1_xxx.dat", ios::out);
    //histogramfile2.open("histogram_2_xxx.dat", ios::out);
    radial_distributionfile.open("radial_distribution_function_xxx.dat", ios::out);

    for(int h=0; h < n_resolution; h++){
        //histogramfile << r_plotting_points(h) << " " << (histogram(h)/pow(r_plotting_points(h),2)) << endl;
        //histogramfile2 << r_plotting_points(h) << " " << histogram(h)/(number_of_bounded_stars) << endl;
    }
    //histogramfile.close();
    //histogramfile2.close();

    radial_averange = sum_radial_single/number_of_bounded_stars;

    double deviation_q1 = sum_radial_single_sqare/number_of_bounded_stars;
    double deviation_q2 = pow((radial_averange),2);
    double deviation = sqrt(deviation_q1-deviation_q2);

    cout << "Averange radial distance: " << radial_averange << endl;
    cout << "Standard deviation: " << deviation << endl;

    double r0 = 2.2;// proporsjonality: pow(N_total,(-1.0/3.0));
    double n0 = 122.0;// proporsjonality: pow(N_total,2);

    vec n = zeros(n_resolution);
    for(int i=0; i < n_resolution; i++){
        n(i) = n0/(1.0+pow((r_plotting_points(i)/r0),4));
    }

    for(int i=0; i < n_resolution; i++){
        radial_distributionfile << r_plotting_points(i) << " " << n(i) << endl;
    }
    radial_distributionfile.close();

}


int main(){

    int N, n_stars;
    double dt, tau_crunch_value, tau_crunch_number, tau_crunch, potential, kinetic, ep_escaped, ek_escaped;
    double masscenter_position_x,  masscenter_position_y,  masscenter_position_z, escaped;

    n_stars = 1000; // number of stars in cluster
    dt = 0.001; // length of steps in numerical methods
    tau_crunch_number = 3.0; // time of simulation

    double my =  10.0; // solar masses mean value
    double R0 = 20.0; // max initial radius Cluster
    double V = (4*pi*pow(R0, 3))/3.0; // volum initial sphere
    double rho0 = (n_stars*my)/V; // particle density
    double G = (3*pi)/(32.0*rho0); // Gravitational constant

    tau_crunch_value = sqrt((3*pi)/(32.0*G*rho0)); // value dimensionless time unit
    tau_crunch = tau_crunch_number*tau_crunch_value; // time unit

    N = (tau_crunch)/dt; //number of timesteps

    fstream outfile1, outfile2, outfile3, outfile4, outfile5;
    outfile1.open("cluster2.dat", ios::out);
    outfile2.open("energy_time_picture_RK4.dat", ios::out);
    outfile3.open("energy_rate.dat", ios::out);
    outfile4.open("number_escaped_xxx.dat", ios::out);
    outfile5.open("energy_escaped_rel_xxx.dat", ios::out);

    // Call Solver
    Solver Cluster = Solver(N, dt, G); // give N and dt;

    // Adding uniformly distibuted stars in a sphere
    Cluster.Stars(n_stars);

    // Initial_values stars(x, y, z, vx, vy, vz)
    Cluster.initial_func();

    escaped=0.0;

    // Does the Leap-Frog or Runge-Kutta4 method N times on initialvalues and write positions and other parameters to file
    #pragma omp parallel for
    for(int k=0; k < N; k++){
        Cluster.LF();

        for(int p=0; p < 3*n_stars; p+=3){
            outfile1 << Cluster.lf1(p) << " " << Cluster.lf1(p+1) << " " << Cluster.lf1(p+2) << " ";
        }
        outfile1 << endl;
/*
        outfile2 << (k*dt) << " ";
        outfile2 << Cluster.Energy(&escaped, &potential, &kinetic, &ep_escaped, &ek_escaped) + ep_escaped + ek_escaped << endl;

        outfile3 << (k*dt) << " ";
        outfile3 << -potential/kinetic << endl;

        outfile4 << (k*dt) << " ";
        outfile4 << escaped << endl;

        outfile5 << (k*dt) << " ";
        outfile5 <<  (100.0*fabs(-ep_escaped - ek_escaped))/Cluster.Energy(&escaped, &potential, &kinetic, &ep_escaped, &ek_escaped) << endl;
*/
    }

    // Use the Masscenter function to find radial density and distribution after the simulation
    Cluster.Masscenter(&masscenter_position_x, &masscenter_position_y, &masscenter_position_z);

    // Loop to write out the random picked masses, sum up and compute the averange mass
/*
    for(int i=0; i<n_stars; i++){
        cout << Cluster.starlist[i].M << endl;
    }

    double sum_masses = 0.0;
    for(int i=0; i<n_stars; i++){
        sum_masses += Cluster.starlist[i].M;
    }
    cout << sum_masses/n_stars << endl;
*/

    outfile1.close();
    outfile2.close();
    outfile3.close();
    outfile4.close();
    outfile5.close();
    return 0;
}




